# Changelog

## 2016-10-xx Framaboard 0.4

- Support Kanboard 1.0.33
- Fix redirection from homepage to an existing account
- Add a gitignore file for user directories
- Fix Framasoft plugin to call correct Framanav's URL

## 2016-02-10 Framaboard 0.3

- Support Kanboard 1.0.25
- Fix bug with pagination in administration
- Few fixes in administration
- Make subdomain redirection form case-insensitive
- Provide a plugin to handle the "Framasoftisation"

## 2015-12-01 Framaboard 0.2

- Make get_account_name working with Nginx
- Add a line to uncomment for specific Framasoft config
- Add a new form to be redirected to user account [#11](https://git.framasoft.org/marien.fressinaud/Framaboard/issues/11)
- Change colors on buttons and title
- Improve documentation [#2](https://git.framasoft.org/marien.fressinaud/Framaboard/issues/2)
- Provide admin section

## 2015-08-26 Framaboard 0.1

- First version
