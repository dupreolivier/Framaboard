"use strict";

$(document).ready(function() {
    var account_manual_change = false;

    $('#subdomain').keyup(function() {
        if (account_manual_change) {
            return;
        }

        var subdomain = $('#subdomain').val();
        $('#account').val(subdomain);
    });

    $('#account').change(function() {
        account_manual_change = ($('#account').val() !== '');
    });

    $('[data-toggle="tooltip"]').tooltip();
});
