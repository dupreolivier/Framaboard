<!DOCTYPE html>
<html>
    <head>
        <title><?php echo TITLE; ?></title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0" />
        <script src="https://framasoft.org/nav/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="https://framasoft.org/nav/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="css/default.css" media="all" rel="stylesheet">
        <link href="css/frama.board.css" media="all" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function(){
        $("#play-pause a").on('click', function() {
            if(jQuery(this).children('.glyphicon').hasClass('glyphicon-pause')) {
                jQuery(this).children('.glyphicon').addClass('glyphicon-play').removeClass('glyphicon-pause');
                jQuery(this).attr('title','Lecture');
                jQuery(this).children('.sr-only').text('Lecture');
                jQuery('#carousel-actus').carousel('pause');
            } else {
                jQuery(this).children('.glyphicon').addClass('glyphicon-pause').removeClass('glyphicon-play');
                jQuery(this).attr('title','Pause');
                jQuery(this).children('.sr-only').text('Pause');
                jQuery('#carousel-actus').carousel('cycle');
            };
            return false;
        });

        $('#login').not('.active').hide();
        $('#registration').not('.active').hide();
        $('a[href*=login]').on('click', function() {
            $('#registration').hide();
            $('#login').slideDown();
            return false;
        });
        $('a[href*=registration]').on('click', function() {
            $('#login').hide();
            $('#registration').slideDown();
            return false;
        });
    });
</script>
    </head>
    <body class="fb_g2">
        <script src="https://framasoft.org/nav/nav.js" type="text/javascript"></script>
        <div class="container ombre">
            <div style="margin-bottom:20px">
                <?php
                    $title = TITLE;
                    if (starts_with($title, 'Frama')) {
                        $title = '<span class="frama">Frama</span>'
                               . '<span class="vert">' . substr($title, 5) . '</span>';
                    }
                ?>
                <h1 class="big-title"><a href="//<?php echo URL_BASE; ?>"><?php echo $title; ?></a></h1>
                <p class="lead">La gestion de projets collaborative</p>
                <hr class="trait" role="presentation" />

                <div class="row">
                    <div class="col-md-12">
                        <!--  Carousel -->
                        <div id="carousel-board" class="carousel slide" role="presentation">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="img/slide-tableau.png" alt="" />
                                    <div class="carousel-caption">
                                        <p>
                                            <strong><?php echo $title; ?></strong> est un gestionnaire de tâches visuel.
                                            Il permet de gérer des projets de manière collaborative, en suivant la méthode Kanban.
                                            Son système visuel permet de s’y retrouver au premier coup d’œil, quelle que soit votre habitude à utiliser ce genre d’outil.
                                        </p>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="img/slide-vues.png" alt="" />
                                    <div class="carousel-caption">
                                        <p>Différentes vues sont proposées afin d’obtenir une vue globale de vos projets.</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="img/slide-backend.png" alt="" />
                                    <div class="carousel-caption">
                                        <p><strong><?php echo $title; ?></strong> est très complet, n’hésitez pas à approfondir ses fonctionnalités pour en profiter pleinement.</p>
                                    </div>
                                </div>
                            </div><!-- /.carousel-inner -->

                            <!-- Controls -->
                            <p class="text-center" id="play-pause"><a href="#play-pause" class="carousel-control" title="Pause"><span class="glyphicon glyphicon-pause"></span><span class="sr-only">Pause</span></a></p>

                            <a class="left carousel-control" href="#carousel-board" role="button" data-slide="prev" title="Diapo précédente">
                                <i class="glyphicon glyphicon-chevron-left"></i><span class="sr-only">Diapo précédente</span>
                            </a>

                            <a class="right carousel-control" href="#carousel-board" role="button" data-slide="next" title="Diapo suivante">
                                <i class="glyphicon glyphicon-chevron-right"></i><span class="sr-only">Diapo suivante</span>
                            </a>
                        </div><!-- /.carousel -->

                        <div class="col-sm-6 text-center">
                            <a href="#login" class="btn btn-primary btn-lg">
                                <i class="glyphicon glyphicon-lock"></i> Se connecter »
                            </a>
                        </div>
                        <div class="col-sm-6 text-center">
                            <a href="#registration" class="btn btn-success btn-lg">
                                <i class="glyphicon glyphicon-user"></i> Créer un compte »
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row <?php echo isset($error) ? 'active' : ''; ?>" id="registration">
                    <div class="col-md-12">
                        <h2>Créer un compte <?php echo TITLE; ?></h2>

                        <?php if (isset($error)) { ?>
                        <div class="col-md-8 col-md-offset-2">
                            <div class="alert alert-danger"><strong>Mince&nbsp;!</strong> <?php echo $error; ?></div>
                        </div>
                        <?php } ?>

                        <div class="col-md-8 col-md-offset-2">
                            <form class="form-horizontal" method="POST" action="#registration" role="form">
                                <div class="alert alert-info">Merci de remplir tous les champs.</div>

                                <div class="form-group <?php echo $test_values === 'subdomain' ? 'has-error' : ''; ?>">
                                    <label class="col-sm-4 control-label" for="subdomain"><span data-toggle="tooltip" title="Votre compte est un espace qui vous est dédié dans lequel vous avez tous les droits." class="glyphicon glyphicon-question-sign"></span> Nom de votre compte<br /><span class="small">(max. 25 caractères)</span></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control" required="" id="subdomain" name="subdomain" pattern="[a-zA-Z0-9]([\w\-]{0,23}[a-zA-Z0-9])?" value="<?php echo isset($_POST['subdomain']) ? $_POST['subdomain'] : ''; ?>" >
                                            <div class="input-group-addon">.<?php echo strlen(URL_BASE) > 20 ? substr(URL_BASE, 0, 20) . '…' : URL_BASE; ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group <?php echo $test_values === 'account' ? 'has-error' : ''; ?>">
                                    <label class="col-sm-4 control-label" for="account">Nom d&rsquo;utilisateur<br /><span class="small">(max. 25 caractères)</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" required="" id="account" name="account" pattern="[\w\-]{1,25}" value="<?php echo isset($_POST['account']) ? $_POST['account'] : ''; ?>" >
                                    </div>
                                </div>
                                <div class="form-group <?php echo $test_values === 'email' ? 'has-error' : ''; ?>">
                                    <label class="col-sm-4 control-label" for="email">Adresse courriel</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" required="" id="email" name="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
                                    </div>
                                </div>
                                <div class="form-group <?php echo $test_values === 'password' ? 'has-error' : ''; ?>">
                                    <label class="col-sm-4 control-label" for="password">Mot de passe</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" required="" id="password" name="password" value="<?php echo isset($_POST['password']) ? $_POST['password'] : ''; ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <input class="btn btn-primary" type="submit" value="Créer mon compte" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row <?php echo isset($error_redirection) ? 'active' : ''; ?>" id="login">
                    <div class="col-md-12">
                        <h2>Vous possédez déjà un compte&nbsp;?</h2>

                        <?php if (isset($error_redirection)) { ?>
                        <div class="col-md-8 col-md-offset-2">
                            <div class="alert alert-danger"><strong>Mince&nbsp;!</strong> <?php echo $error_redirection; ?></div>
                        </div>
                        <?php } ?>

                        <div class="col-md-8 col-md-offset-2">
                            <form class="form-horizontal" method="POST" action="#login" role="form">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="subdomain_redirect">Votre compte</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control" required="" id="subdomain_redirect" name="subdomain_redirect" pattern="[a-zA-Z0-9]([\w\-]{0,23}[a-zA-Z0-9])?" value="<?php echo isset($_POST['subdomain_redirect']) ? $_POST['subdomain_redirect'] : ''; ?>">
                                            <div class="input-group-addon">.<?php echo strlen(URL_BASE) > 20 ? substr(URL_BASE, 0, 20) . '…' : URL_BASE; ?></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <input class="btn btn-primary" type="submit" value="Accéder à mon compte" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <hr role="presentation" />

                <div class="row">
                    <div id="tuto-faq" class="col-md-4">
                        <h2>Prise en main</h2>
                        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-question-sign"></span></p>
                        <div id="aboutbox">
                            <p><span class="framalink"><?php echo URL_BASE; ?></span> est un service en ligne libre de gestion de tâches.</p>
                            <ol>
                                <li>Créez votre compte&nbsp;;</li>
                                <li>Connectez-vous à votre nouveau compte&nbsp;;</li>
                                <li>Commencez à gérer vos projets à travers votre Kanboard&nbsp;;</li>
                                <li>Lisez <a href="https://github.com/fguillot/kanboard/blob/master/doc/fr_FR/index.markdown">la documentation</a> pour en savoir plus.</li>
                            </ol>
                        </div>
                    </div>

                    <div class="col-md-4" id="le-logiciel">
                        <h2>Le logiciel</h2>
                        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-cloud"></span></p>
                        <p><span class="framalink"><?php echo URL_BASE; ?></span> est une instance du logiciel <a href="http://kanboard.net/"><abbr>Kanboard</abbr></a> développé par <a href="https://github.com/fguillot">Frédéric Guillot</a>.</p>
                        <p><abbr>Kanboard</abbr> est sous licence <a href="http://opensource.org/licenses/MIT"><abbr title="Massachusetts Institute of Technology" lang="en">MIT</abbr></a>.</p>
                    </div>

                    <div class="col-md-4" id="jardin">
                        <h2>Cultivez votre jardin</h2>
                        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-tree-deciduous"></span></p>
                        <p>Pour participer au développement du logiciel, proposer des améliorations
                           ou simplement le télécharger, rendez-vous sur <a href="https://github.com/fguillot/kanboard">le site de développement</a>.</p>
                        <p>Si vous souhaitez installer ce logiciel pour votre propre usage et ainsi gagner en autonomie, nous vous aidons sur&nbsp;:</p>
                        <p class="text-center"><a href="http://framacloud.org/cultiver-son-jardin/installation-de-kanboard/" class="btn btn-success"><span class="glyphicon glyphicon-tree-deciduous"></span> framacloud.org</a></p>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/frama.board.js" type="text/javascript"></script>
    </body>
</html>
